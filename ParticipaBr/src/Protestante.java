
public class Protestante {
	protected String nome;
	protected String email;
	protected String senha;
	protected String reinvidicacao;
	
	public Protestante(String nome, String email, String senha){
		this.nome = nome;
		this.email = email;
		this.senha = senha;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getReinvidicacao() {
		return reinvidicacao;
	}
	public void setReinvidicacao(String reinvidicacao) {
		this.reinvidicacao = reinvidicacao;
	}
	
	
}
