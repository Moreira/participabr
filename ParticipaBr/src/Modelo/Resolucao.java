package Modelo;

public class Resolucao {
	
	
	private static String resolucao;

	public Resolucao(String resolucao){
		this.resolucao = resolucao;
	}
	
	public static String getResolucao() {
		return resolucao;
	}

	public void setResolucao(String resolucao) {
		this.resolucao = resolucao;
	}
	
}
