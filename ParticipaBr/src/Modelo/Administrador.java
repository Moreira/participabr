package Modelo;
import java.util.ArrayList;

import Modelo.*;

public class Administrador {
	private static  ArrayList<reinvidicacao> listaReinvidicacoes;
	private ArrayList<Protestante> listaProtestantes;
	private ArrayList<Resolucao> listaResolucoes;

	
	public void adicionar(reinvidicacao umaReinvidicacao){
		listaReinvidicacoes.add(umaReinvidicacao);
	}
	
	public static ArrayList<reinvidicacao> getListaReinvidicacoes() {
		return listaReinvidicacoes;
	}

	public void setListaReinvidicacoes(ArrayList<reinvidicacao> listaReinvidicacoes) {
		this.listaReinvidicacoes = listaReinvidicacoes;
	}

	public void remover(reinvidicacao umaReinvidicacao){
		listaReinvidicacoes.remove(umaReinvidicacao);
	}
	
	public void adicionar(Protestante umProtestante){
		listaProtestantes.add(umProtestante);
	}
	
	public void remover(Protestante umProtestante){
		listaProtestantes.remove(umProtestante);
	}
	
	public void adicionar(Resolucao umaResolucao){
		listaResolucoes.add(umaResolucao);
	}
}
