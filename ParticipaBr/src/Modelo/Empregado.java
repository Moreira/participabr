package Modelo;

import java.util.ArrayList;

public class Empregado extends Administrador{
	private static String nome;
	private static String email;
	private static String senha;
	private String codigo;
	private String resolucao;
	private ArrayList<Resolucao> listaResolucoes;
	
	public Empregado(final String nome, final String email, final String senha){
		this.nome = nome;
		this.email = email;
		this.senha = senha;
	}
	
	public static String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public static String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public static String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getResolucao() {
		return resolucao;
	}
	public void setResolucao(String resolucao) {
		this.resolucao = resolucao;
	}
	
	public void adicionar(Resolucao umaResolucao){
		listaResolucoes.add(umaResolucao);
	}
	
}
