package Modelo;

import java.util.ArrayList;

public class Protestante {

	protected static String nome;
	protected static String email;
	protected static String senha;
	protected String reinvidicacao;
	private ArrayList<reinvidicacao> listaReinvidicacoes;
	private ArrayList<Protestante> listaProtestantes;
	
	public Protestante(String nome, String email, String senha){
		this.nome = nome;
		this.email = email;
		this.senha = senha;
	}
	
	public static String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public static String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public static String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getReinvidicacao() {
		return reinvidicacao;
	}
	public void setReinvidicacao(String reinvidicacao) {
		this.reinvidicacao = reinvidicacao;
	}
	
	public void adicionar(reinvidicacao umaReinvidicacao){
		listaReinvidicacoes.add(umaReinvidicacao);	
	}
	
	public void adicionar(Protestante umProtestante){
		listaProtestantes.add(umProtestante);
	}
	
}
