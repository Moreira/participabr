package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Modelo.Empregado;
import Modelo.Resolucao;

public class TesteEmpregado {
		
	private Empregado umEmpregado;
	private Resolucao umaResolucao;
	
	@Before
	public void setUp() throws Exception {
		umEmpregado = new Empregado("João", "joao@outlook.com", "555");
		umaResolucao = new Resolucao("melhor qualidade de vida para o povo brasileiro");
	}

	@Test
	public void testNome() {
		assertEquals("João", Empregado.getNome());
	}
	@Test
	public void testEmail(){
		assertEquals("joao@outlook.com", Empregado.getEmail());
	}
	@Test
	public void testSenha(){
		assertEquals("555", Empregado.getSenha());
	}
	@Test
	public void testResolucao(){
		assertEquals("melhor qualidade de vida para o povo brasileiro", Resolucao.getResolucao());
	}
	
	
}
