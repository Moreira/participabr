package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import Modelo.*;

public class TesteProtestante {

	private Protestante umProtestante;

	@Before
	public void setUp() throws Exception {
	umProtestante = new Protestante("Gustavo", "gustavo.mn@outlook.com", "1234");
	
	}

	@Test
	public void testNome() {
		assertEquals("Gustavo", Protestante.getNome());
	}
	@Test
	public void testEmail(){
		assertEquals("gustavo.mn@outlook.com", Protestante.getEmail());
	}
	@Test
	public void testSenha(){
		assertEquals("1234", Protestante.getSenha());
	}
}
